# alpine-php5

#### [alpine-x64-php5](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-php5/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-php5.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-php5 "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-php5.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-php5 "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-php5.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-php5/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-php5.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-php5/)
#### [alpine-aarch64-php5](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-php5/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-php5.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-php5 "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-php5.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-php5 "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-php5.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-php5/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-php5.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-php5/)
#### [alpine-armhf-php5](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-php5/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-php5.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-php5 "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-php5.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-php5 "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-php5.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-php5/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-php5.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-php5/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [PHP](https://secure.php.net/)
    - PHP is a popular general-purpose scripting language that is especially suited to web development.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 9000:9000/tcp \
           forumi0721alpinex64/alpine-x64-php5:latest
```

* aarch64
```sh
docker run -d \
           -p 9000:9000/tcp \
           forumi0721alpineaarch64/alpine-aarch64-php5:latest
```

* armhf
```sh
docker run -d \
           -p 9000:9000/tcp \
           forumi0721alpinearmhf/alpine-armhf-php5:latest
```



----------------------------------------
#### Usage

* URL : [localhost:9000](localhost:9000)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 9000/tcp           | FastCGI server listening port                    |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables
| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-php5](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-php5/)
* [forumi0721alpineaarch64/alpine-aarch64-php5](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-php5/)
* [forumi0721alpinearmhf/alpine-armhf-php5](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-php5/)

